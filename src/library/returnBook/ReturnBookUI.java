package library.returnBook;
import java.util.Scanner;


public class ReturnBookUI {

    public static enum UIState { INITIALISED, READY, INSPECTING, COMPLETED };

    private ReturnBookControl control;
    private Scanner input;
    private UIState state;

    public ReturnBookUI(ReturnBookControl control) {
        this.control = control;
        input = new Scanner(System.in);
        state = UIState.INITIALISED;
        control.setUi(this);
    }


    public void Run() {		
        UIOutput("Return Book Use Case UI\n");

        while (true) {

            switch (state) {

            case INITIALISED:
                break;

            case READY:{
                String bookInputString = UIInput("Scan Book (<enter> completes): ");
                if (bookInputString.length() == 0) {
                    control.scanningComplete();
                }
                

                else {
                    try {
                        int bookId = Integer.valueOf(bookInputString).intValue();
                        control.bookScanned(bookId);
                    }
                    catch (NumberFormatException e) {
                        UIOutput("Invalid bookId");
                    }					
                }
                break;				
            }
            
            case INSPECTING:{
                String answer = UIInput("Is book damaged? (Y/N): ");
                boolean isDamaged = false;
                if (answer.toUpperCase().equals("Y")) { 					
                    isDamaged = true;
                }
                control.dischargeLoan(isDamaged);
            }
            
            case COMPLETED:{
                UIOutput("Return processing complete");
                return;
            }

            default:{
                UIOutput("Unhandled state");
                throw new RuntimeException("ReturnBookUI : unhandled state :" + state);
            }
            }
        }
    }

    private String UIInput(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }	

    private void UIOutput(Object object) {
        System.out.println(object);
    }


    public void Display(Object object) {
        UIOutput(object);
    }

    public void SetState(UIState state) {
        this.state = state;
    }
}
