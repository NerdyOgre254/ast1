package library.returnBook;
import library.entities.Book;
import library.entities.Library;
import library.entities.Loan;

public class ReturnBookControl {

    private ReturnBookUI Ui;
    private enum BookControlState { INITIALISED, READY, INSPECTING };
    private BookControlState controlState;

    private Library library;
    private Loan currentLoan;


    public ReturnBookControl() {
        this.library = Library.getInstance();
        controlState = BookControlState.INITIALISED;
    }


    public void setUi(ReturnBookUI uI) {
        if (!controlState.equals(BookControlState.INITIALISED)) {
            throw new RuntimeException("ReturnBookControl: cannot call setUI except in INITIALISED state");
        }

        this.Ui = uI;
        uI.SetState(ReturnBookUI.UIState.READY);
        controlState = BookControlState.READY;		
    }


    public void bookScanned(int bOoK_iD) {
        if (!controlState.equals(BookControlState.READY)) { 
            throw new RuntimeException("ReturnBookControl: cannot call bookScanned except in READY state");
        }

        Book cUrReNt_bOoK = library.getBook(bOoK_iD);

        if (cUrReNt_bOoK == null) {
            Ui.Display("Invalid Book Id");
            return;
        }
        if (!cUrReNt_bOoK.isOnLoan()) {
            Ui.Display("Book has not been borrowed");
            return;
        }		
        currentLoan = library.getLoanByBookId(bOoK_iD);	
        double Over_Due_Fine = 0.0;

        if (currentLoan.isOverdue()) { 
            Over_Due_Fine = library.calculateOverDueFine(currentLoan);
        }

        Ui.Display("Inspecting");
        Ui.Display(cUrReNt_bOoK.toString());
        Ui.Display(currentLoan.toString());

        if (currentLoan.isOverdue()) { 
            Ui.Display(String.format("\nOverdue fine : $%.2f", Over_Due_Fine));
        }

        Ui.SetState(ReturnBookUI.UIState.INSPECTING);
        controlState = BookControlState.INSPECTING;		
    }


    public void scanningComplete() {
        if (!controlState.equals(BookControlState.READY)) { 
            throw new RuntimeException("ReturnBookControl: cannot call scanningComplete except in READY state");
        }

        Ui.SetState(ReturnBookUI.UIState.COMPLETED);		
    }


    public void dischargeLoan(boolean iS_dAmAgEd) {
        if (!controlState.equals(BookControlState.INSPECTING)) { 
            throw new RuntimeException("ReturnBookControl: cannot call dischargeLoan except in INSPECTING state");
        }

        library.dischargeLoan(currentLoan, iS_dAmAgEd);
        currentLoan = null;
        Ui.SetState(ReturnBookUI.UIState.READY);
        controlState = BookControlState.READY;				
    }
}

//comment code to add in git
