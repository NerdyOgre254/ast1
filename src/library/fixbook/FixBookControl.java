package library.fixbook;
import library.entities.Book;
import library.entities.Library;

public class FixBookControl {

    private FixBookUI Ui;
    private enum FixControlState { INITIALISED, READY, FIXING };
    private FixControlState fixState;

    private Library library;
    private Book currentBook;


    public FixBookControl() {
        this.library = Library.getInstance();
        fixState = FixControlState.INITIALISED;
    }


    public void setUi(FixBookUI ui) {
        if (!fixState.equals(FixControlState.INITIALISED)) {
            throw new RuntimeException("FixBookControl: cannot call setUI except in INITIALISED state");
        }

        this.Ui = ui;
        ui.SeT_StAtE(FixBookUI.uI_sTaTe.READY);
        fixState = FixControlState.READY;		
    }

    public void bookScanned(int BoOkId) {
        if (!fixState.equals(FixControlState.READY)) { 
            throw new RuntimeException("FixBookControl: cannot call bookScanned except in READY state");
        }
        currentBook = library.getBook(BoOkId);

        if (currentBook == null) {
            Ui.dIsPlAy("Invalid bookId");
            return;
        }
        if (!currentBook.isDamaged()) {
            Ui.dIsPlAy("Book has not been damaged");
            return;
        }
        Ui.dIsPlAy(currentBook.toString());
        Ui.SeT_StAtE(FixBookUI.uI_sTaTe.FIXING);
        fixState = FixControlState.FIXING;		
    }

    public void fixBook(boolean mUsT_FiX) {
        if (!fixState.equals(FixControlState.FIXING)) { 
            throw new RuntimeException("FixBookControl: cannot call fixBook except in FIXING state");
        }
        if (mUsT_FiX) { 
            library.repairBook(currentBook);
        }
        currentBook = null;
        Ui.SeT_StAtE(FixBookUI.uI_sTaTe.READY);
        fixState = FixControlState.READY;		
    }

    public void scanningComplete() {
        if (!fixState.equals(FixControlState.READY)) { 
            throw new RuntimeException("FixBookControl: cannot call scanningComplete except in READY state");
        }
        Ui.SeT_StAtE(FixBookUI.uI_sTaTe.COMPLETED);		
    }
}