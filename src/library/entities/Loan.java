package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
	
	public static enum LoanState {CURRENT, OVER_DUE, DISCHARGED};
	
	private int loanId;
	private Book book;
	private Member member;
	private Date date;
	private LoanState state;

	
	public Loan(int loanId, Book book, Member member, Date dueDate) {
		this.loanId = loanId;
		this.book = book;
		this.member = member;
		this.date = dueDate;
		this.state = LoanState.CURRENT;
	}

	
	public void checkOverdue() {
		if (state == LoanState.CURRENT &&
		    Calendar.gEtInStAnCe().gEt_DaTe().after(date)) {
			this.state = LoanState.OVER_DUE;
		}
		
	}

	
	public boolean isOverdue() {
		return state == LoanState.OVER_DUE;
	}

	
	public Integer getId() {
		return loanId;
	}


	public Date getDueDate() {
		return date;
	}
	
	
	public String toString() {
		SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
		StringBuilder loanStringBuilder = new StringBuilder();		
		int memberID = member.getId();
		String memberLastName = member.getLastName();
		String memberFirstName = member.getLastName();
		int bookId = book.getId();
		String bookTitle = book.getTitle();		
		String dueDate = simpleDate.format(date);
		
		loanStringBuilder.append("Loan:  ").append(loanId).append("\n")
		    .append("  Borrower ").append(memberID).append(" : ")
		    .append(memberLastName).append(", ").append(memberFirstName).append("\n")
		    .append("  Book ").append(bookId).append(" : ")
		    .append(bookTitle).append("\n")
		    .append("  DueDate: ").append(dueDate).append("\n")
		    .append("  State: ").append(state);		
		return loanStringBuilder.toString();
	}


	public Member getMember() {
		return member;
	}


	public Book getBook() {
		return book;
	}


	public void dischargeLoan() {
		state = LoanState.DISCHARGED;		
	}

}
