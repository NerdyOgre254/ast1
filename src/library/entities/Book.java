package library.entities;
import java.io.Serializable;


@SuppressWarnings("serial")
public class Book implements Serializable {

    private String title;
    private String author;
    private String callNumber;
    private int bookId;

    private enum BookState { AVAILABLE, ON_LOAN, DAMAGED, RESERVED };
    private BookState bookState;


    public Book(String author, String title, String callNumber, int bookId) {
        this.author = author;
        this.title = title;
        this.callNumber = callNumber;
        this.bookId = bookId;
        this.bookState = BookState.AVAILABLE;
    }

    public String toString() {
        StringBuilder stringBuild = new StringBuilder();
        stringBuild.append("Book: ").append(bookId).append("\n")
                   .append("  Title:  ").append(title).append("\n")
                   .append("  Author: ").append(author).append("\n")
                   .append("  CallNo: ").append(callNumber).append("\n")
                   .append("  State:  ").append(bookState);

        return stringBuild.toString();
    }

    public Integer getId() {
        return bookId;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAvailable() {
        return bookState == BookState.AVAILABLE;
    }

    public boolean isOnLoan() {
        return bookState == BookState.ON_LOAN;
    }

    public boolean isDamaged() {
        return bookState == BookState.DAMAGED;
    }

    public void borrow() {
        if (bookState.equals(BookState.AVAILABLE)) {
            bookState = BookState.ON_LOAN;
        }
        else { 
            throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", bookState));
        }
    }


    public void returnBook(boolean damaged) {
        if (bookState.equals(BookState.ON_LOAN)) 
            if (damaged) {
                bookState = BookState.DAMAGED;
            }
            else { 
                bookState = BookState.AVAILABLE;
            }
        else {
            throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", bookState));
        }
    }

    public void repair() {
        if (bookState.equals(BookState.DAMAGED)) {
            bookState = BookState.AVAILABLE;
        }
        else { 
            throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", bookState));
        }
    }
}
